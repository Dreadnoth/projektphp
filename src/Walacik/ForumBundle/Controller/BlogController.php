<?php

namespace Walacik\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Walacik\ForumBundle\Form\CommentBoxType;
use Walacik\AdminBundle\Entity\Comment;

/**
 * @Route("/blog")
 */
class BlogController extends Controller{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction(){
        $em = $this->getDoctrine()->getManager();
        $entries = $em->getRepository('WalacikAdminBundle:Blogentry')->findBy(array(),array('datadodania' => 'ASC'));
        return array('entries' => $entries);
    }
}