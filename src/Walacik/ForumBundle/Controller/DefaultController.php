<?php

namespace Walacik\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Walacik\ForumBundle\Form\CommentBoxType;
use Walacik\AdminBundle\Entity\Comment;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('WalacikAdminBundle:Bed');
        $beds = $repo->findAll();
        return array('beds' => $beds);
    }
    /**
     * @Route("/show/{id}")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id){
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('WalacikAdminBundle:Bed');
        
        $entity = $repo->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bed entity.');
        }
        
        $form = $this->createForm(new CommentBoxType(), null,array(
            'action' => $this->generateUrl('comment_create',array('id' => $entity->getPatient()->getId())),
            'method' => 'POST',
        ))->add('submit','submit',array('attr' => array('class'=>'btn btn-info')));
        
        $comments = $entity->getPatient()->getComments();
        
        return array('bed' => $entity,'commentBox' => $form->createView(),'comments' => $comments);
    }
    /**
     * @Template("WalacikForumBundle:Default:show.html.twig")
     * @Method("POST")
     * @Route("/show/{id}",name="comment_create")
     */
    public function addComentAction($id,Request $request){
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('WalacikAdminBundle:Patient');
        
        $entity = $repo->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Patient entity.');
        }
        
        $comment = new Comment();
        $form = $this->createForm(new CommentBoxType(), $comment,array(
            'action' => $this->generateUrl('comment_create',array('id' => $id)),
            'method' => 'POST',
        ))->add('submit','submit',array());
        $form->handleRequest($request);
        if($form->isValid()){
            $comment->setAuthor($this->getUser());
            $comment->setDatadodania(new \DateTime());
            $comment->setPatient($entity);
            
            $em->persist($comment);
            $em->flush();
        }
        $comments = $entity->getComments();
        
        return array('bed' => $entity->getBed(),'commentBox' => $form->createView(),'comments' => $comments);
    }
}
