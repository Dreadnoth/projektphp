<?php

namespace Walacik\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Walacik\AdminBundle\Entity\Disease;
use Walacik\AdminBundle\Form\DiseaseType;

/**
 * Disease controller.
 *
 * @Route("/disease")
 */
class DiseaseController extends Controller
{

    /**
     * Lists all Disease entities.
     *
     * @Route("/", name="disease")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WalacikAdminBundle:Disease')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Disease entity.
     *
     * @Route("/", name="disease_create")
     * @Method("POST")
     * @Template("WalacikAdminBundle:Disease:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Disease();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('disease_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Disease entity.
    *
    * @param Disease $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Disease $entity)
    {
        $form = $this->createForm(new DiseaseType(), $entity, array(
            'action' => $this->generateUrl('disease_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Utwórz','attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new Disease entity.
     *
     * @Route("/new", name="disease_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Disease();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Disease entity.
     *
     * @Route("/{id}", name="disease_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WalacikAdminBundle:Disease')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Disease entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Disease entity.
     *
     * @Route("/{id}/edit", name="disease_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WalacikAdminBundle:Disease')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Disease entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Disease entity.
    *
    * @param Disease $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Disease $entity)
    {
        $form = $this->createForm(new DiseaseType(), $entity, array(
            'action' => $this->generateUrl('disease_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Aktualizuj','attr' => array('class' => 'btn btn-primary')));

        return $form;
    }
    /**
     * Edits an existing Disease entity.
     *
     * @Route("/{id}", name="disease_update")
     * @Method("PUT")
     * @Template("WalacikAdminBundle:Disease:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WalacikAdminBundle:Disease')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Disease entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('disease_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Disease entity.
     *
     * @Route("/{id}", name="disease_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WalacikAdminBundle:Disease')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Disease entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('disease'));
    }

    /**
     * Creates a form to delete a Disease entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('disease_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Usuń','attr' => array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }
}
