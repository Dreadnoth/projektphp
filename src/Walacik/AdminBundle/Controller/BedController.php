<?php

namespace Walacik\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Walacik\AdminBundle\Entity\Bed;
use Walacik\AdminBundle\Form\BedType;

/**
 * Bed controller.
 *
 * @Route("/bed")
 */
class BedController extends Controller
{

    /**
     * Lists all Bed entities.
     *
     * @Route("/", name="bed")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WalacikAdminBundle:Bed')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Bed entity.
     *
     * @Route("/", name="bed_create")
     * @Method("POST")
     * @Template("WalacikAdminBundle:Bed:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Bed();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('bed_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Bed entity.
    *
    * @param Bed $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Bed $entity)
    {
        $form = $this->createForm(new BedType(), $entity, array(
            'action' => $this->generateUrl('bed_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Bed entity.
     *
     * @Route("/new", name="bed_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Bed();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Bed entity.
     *
     * @Route("/{id}", name="bed_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WalacikAdminBundle:Bed')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bed entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Bed entity.
     *
     * @Route("/{id}/edit", name="bed_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WalacikAdminBundle:Bed')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bed entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Bed entity.
    *
    * @param Bed $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Bed $entity)
    {
        $form = $this->createForm(new BedType(), $entity, array(
            'action' => $this->generateUrl('bed_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Bed entity.
     *
     * @Route("/{id}", name="bed_update")
     * @Method("PUT")
     * @Template("WalacikAdminBundle:Bed:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WalacikAdminBundle:Bed')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bed entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('bed_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Bed entity.
     *
     * @Route("/{id}", name="bed_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WalacikAdminBundle:Bed')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Bed entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('bed'));
    }

    /**
     * Creates a form to delete a Bed entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bed_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
