<?php

namespace Walacik\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bed
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Bed
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\OneToOne(targetEntity="Patient",inversedBy="bed")
     * @ORM\JoinColumn(name="patient_id", referencedColumnName="id")
     */
    private $patient;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set patient
     *
     * @param \Walacik\AdminBundle\Entity\Patient $patient
     * @return Bed
     */
    public function setPatient(\Walacik\AdminBundle\Entity\Patient $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient
     *
     * @return \Walacik\AdminBundle\Entity\Patient 
     */
    public function getPatient()
    {
        return $this->patient;
    }
}
