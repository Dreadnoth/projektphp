<?php

namespace Walacik\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comment
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Comment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tresc", type="text")
     */
    private $tresc;

    /**
     * @ORM\ManyToOne(targetEntity="Patient", inversedBy="comments")
     * @ORM\JoinColumn(name="patient_id", referencedColumnName="id")
     */
    private $patient;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="comments")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     **/
    private $author;
    /**
     * @ORM\Column(name="datadodania",type="datetime",nullable=false)
     */
    private $datadodania;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tresc
     *
     * @param string $tresc
     * @return Comment
     */
    public function setTresc($tresc)
    {
        $this->tresc = $tresc;

        return $this;
    }

    /**
     * Get tresc
     *
     * @return string 
     */
    public function getTresc()
    {
        return $this->tresc;
    }

    /**
     * Set patient
     *
     * @param \Walacik\AdminBundle\Entity\Patient $patient
     * @return Comment
     */
    public function setPatient(\Walacik\AdminBundle\Entity\Patient $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient
     *
     * @return \Walacik\AdminBundle\Entity\Patient 
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * Set author
     *
     * @param \Walacik\AdminBundle\Entity\User $author
     * @return Comment
     */
    public function setAuthor(\Walacik\AdminBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \Walacik\AdminBundle\Entity\User 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set datadodania
     *
     * @param \DateTime $datadodania
     * @return Comment
     */
    public function setDatadodania($datadodania)
    {
        $this->datadodania = $datadodania;

        return $this;
    }

    /**
     * Get datadodania
     *
     * @return \DateTime 
     */
    public function getDatadodania()
    {
        return $this->datadodania;
    }
}
