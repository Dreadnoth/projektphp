<?php

namespace Walacik\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BlogEntry
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class BlogEntry
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tytul", type="string", length=255)
     */
    private $tytul;

    /**
     * @var string
     *
     * @ORM\Column(name="tresc", type="text")
     */
    private $tresc;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datadodania", type="datetime")
     */
    private $datadodania;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tytul
     *
     * @param string $tytul
     * @return BlogEntry
     */
    public function setTytul($tytul)
    {
        $this->tytul = $tytul;

        return $this;
    }

    /**
     * Get tytul
     *
     * @return string 
     */
    public function getTytul()
    {
        return $this->tytul;
    }

    /**
     * Set tresc
     *
     * @param string $tresc
     * @return BlogEntry
     */
    public function setTresc($tresc)
    {
        $this->tresc = $tresc;

        return $this;
    }

    /**
     * Get tresc
     *
     * @return string 
     */
    public function getTresc()
    {
        return $this->tresc;
    }

    /**
     * Set datadodania
     *
     * @param \DateTime $datadodania
     * @return BlogEntry
     */
    public function setDatadodania($datadodania)
    {
        $this->datadodania = $datadodania;

        return $this;
    }

    /**
     * Get datadodania
     *
     * @return \DateTime 
     */
    public function getDatadodania()
    {
        return $this->datadodania;
    }
}
