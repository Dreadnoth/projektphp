<?php

namespace Walacik\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Patient
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Patient
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="imie", type="string", length=255)
     */
    private $imie;

    /**
     * @var string
     *
     * @ORM\Column(name="nazwisko", type="string", length=255)
     */
    private $nazwisko;
    
    /**
     * @ORM\ManyToMany(targetEntity="Disease",inversedBy="patients")
     * @ORM\JoinTable(name="patient_disease")
     */
    private $diseases;
    /**
     * @ORM\OneToOne(targetEntity="Bed", mappedBy="patient")
     */
    private $bed;
    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="patient")
     * @ORM\JoinColumn(name="comment_id", referencedColumnName="id")
     */
    private $comments;
    
    public function __construct() {
        $this->comments = new ArrayCollection();
        $this->diseases = new ArrayCollection();
    }

        public function __toString() {
        return $this->imie.' '.$this->nazwisko;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set imie
     *
     * @param string $imie
     * @return Patient
     */
    public function setImie($imie)
    {
        $this->imie = $imie;

        return $this;
    }

    /**
     * Get imie
     *
     * @return string 
     */
    public function getImie()
    {
        return $this->imie;
    }

    /**
     * Set nazwisko
     *
     * @param string $nazwisko
     * @return Patient
     */
    public function setNazwisko($nazwisko)
    {
        $this->nazwisko = $nazwisko;

        return $this;
    }

    /**
     * Get nazwisko
     *
     * @return string 
     */
    public function getNazwisko()
    {
        return $this->nazwisko;
    }

    /**
     * Set bed
     *
     * @param \Walacik\AdminBundle\Entity\Bed $bed
     * @return Patient
     */
    public function setBed(\Walacik\AdminBundle\Entity\Bed $bed = null)
    {
        $this->bed = $bed;

        return $this;
    }

    /**
     * Get bed
     *
     * @return \Walacik\AdminBundle\Entity\Bed 
     */
    public function getBed()
    {
        return $this->bed;
    }

    /**
     * Add diseases
     *
     * @param \Walacik\AdminBundle\Entity\Disease $diseases
     * @return Patient
     */
    public function addDisease(\Walacik\AdminBundle\Entity\Disease $diseases)
    {
        $diseases->addPatient($this);
        $this->diseases[] = $diseases;

        return $this;
    }

    /**
     * Remove diseases
     *
     * @param \Walacik\AdminBundle\Entity\Disease $diseases
     */
    public function removeDisease(\Walacik\AdminBundle\Entity\Disease $diseases)
    {
        $this->diseases->removeElement($diseases);
    }

    /**
     * Get diseases
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDiseases()
    {
        return $this->diseases;
    }

    /**
     * Add comments
     *
     * @param \Walacik\AdminBundle\Entity\Comment $comments
     * @return Patient
     */
    public function addComment(\Walacik\AdminBundle\Entity\Comment $comments)
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comments
     *
     * @param \Walacik\AdminBundle\Entity\Comment $comments
     */
    public function removeComment(\Walacik\AdminBundle\Entity\Comment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComments()
    {
        return $this->comments;
    }
}
