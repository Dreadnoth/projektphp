<?php

namespace Walacik\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CreateUserType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username',null,array('label' => 'Nazwa użytkownika'))
            ->add('email','email',array('label' => 'Adres e-mail'))
            ->add('plainPassword','repeated',array(
                'type' => 'password',
                'invalid_message' => 'Hasła muszą pasować',
                'required' => true,
                'first_options'  => array('label' => 'Hasło'),
                'second_options' => array('label' => 'Powtórz hasło'),
            ))
            ->add('type','choice',array('label' => 'Typ konta','choices' => array('ROLE_ADMIN' => 'Administrator','ROLE_PIELEGNIARKA' => 'Pielegniarka','ROLE_LEKARZ' => 'Lekarz')))
            //->add('datadodania')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Walacik\AdminBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'walacik_adminbundle_createusertype';
    }
}
