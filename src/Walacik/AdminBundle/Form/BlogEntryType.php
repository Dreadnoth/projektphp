<?php

namespace Walacik\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BlogEntryType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tytul')
            ->add('tresc',null,array('attr' => array('class' => 'tinymce')))
            //->add('datadodania')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Walacik\AdminBundle\Entity\BlogEntry'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'walacik_adminbundle_blogentry';
    }
}
